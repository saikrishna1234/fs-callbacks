let fs = require("fs");

function problem2(data) {
  let upperCaseData = data.toUpperCase();
  fs.writeFile("storeData/upperCaseData.txt", upperCaseData, (err) => {
    if (err) throw err;
  });

  fs.appendFile(
    "storeData/filenames.txt",
    `storeData/upperCaseData.txt\n`,
    (err) => {
      if (err) throw err;

      fs.readFile("storeData/upperCaseData.txt", "utf-8", (err, data) => {
        if (err) {
          console.log(err);
        } else {
          let lowerCase = data.toLowerCase();
          let sentence = lowerCase.split(".").join("\n");
          fs.writeFile("storeData/splittedData.txt", sentence, (err) => {
            if (err) throw err;
          });
          fs.appendFile(
            "storeData/filenames.txt",
            "storeData/splittedData.txt\n",
            (err) => {
              if (err) throw err;

              fs.readFile(
                "storeData/splittedData.txt",
                "utf-8",
                (err, data) => {
                  if (err) {
                    console.log(err);
                  } else {
                    let sortedData = data.split("\n").sort().join("\n");

                    fs.writeFile(
                      "storeData/sortedData.txt",
                      sortedData,
                      (err) => {
                        if (err) throw err;
                      }
                    );

                    fs.appendFile(
                      "storeData/filenames.txt",
                      "storeData/sortedData.txt\n",
                      (err) => {
                        if (err) throw err;

                        fs.readFile(
                          "storeData/filenames.txt",
                          "utf-8",
                          (err, data) => {
                            if (err) throw err;
                            let fileNames = data.split("\n");
                            fileNames.map((each) => {
                              fs.unlink(each, (err) => {
                                if (err) throw err;
                                else {
                                  console.log("files Deleted");
                                }
                              });
                            });
                          }
                        );
                      }
                    );
                  }
                }
              );
            }
          );
        }
      });
    }
  );
}

module.exports = problem2;
