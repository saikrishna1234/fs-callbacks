let fs = require("fs");
const path = require("path");

function problem1(fileName, callBack) {
  fs.mkdir(fileName, (err) => {
    callBack(err, "Directory successfully created");

    const randomFiles = [1, 2, 3, 4, 5, 6, 7, 8];

    randomFiles.forEach((number) => {
      let fileName = "file" + number + ".json";
      let jsonFileData = {
        id: number,
        name: fileName,
      };

      fs.writeFile(
        path.join("jsonFolder", fileName),
        JSON.stringify(jsonFileData),
        (err) => {
          callBack(err, "Successfully added json file");

          fs.unlink(path.join("./jsonFolder", fileName), (err) => {
            callBack(err, "Deleted file Successfully");
          });
        }
      );
    });
  });
}

module.exports = problem1;
