const fs = require("fs");
const problem2 = require("../problem2.js");

const data = fs.readFile("lipsum.txt", "utf-8", (err, data) => {
  if (err) {
    console.log(err);
  }
  problem2(data);
});
