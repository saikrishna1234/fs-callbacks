let problem1 = require("../problem1.js");

function callBack(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
}

problem1("jsonFolder", callBack);
